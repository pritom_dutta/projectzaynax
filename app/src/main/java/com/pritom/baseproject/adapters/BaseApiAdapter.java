package com.pritom.baseproject.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.pritom.baseproject.data.model.api.ItemModel;
import com.pritom.baseproject.databinding.ItemRecycleBinding;
import com.pritom.baseproject.ui.activity.main.ApiItemViewModel;
import com.pritom.baseproject.ui.base.BaseViewHolder;

import java.util.List;

public class BaseApiAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;


    private List<ItemModel> list;

    public BaseApiAdapter(List<ItemModel> list) {
        this.list = list;
    }


    @Override
    public int getItemCount() {
        if (list != null && list.size() > 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_NORMAL;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemRecycleBinding item = ItemRecycleBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new BaseApiHolder(item);
    }


    public void addItems(List<ItemModel> list) {
        Log.e("DATALINK", "onSuccessData: "+list.size() );
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearItems() {
        list.clear();
    }


    public class BaseApiHolder extends BaseViewHolder {

        private ItemRecycleBinding mBinding;

        private ApiItemViewModel mApiItemViewModel;

        public BaseApiHolder(ItemRecycleBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            mApiItemViewModel = new ApiItemViewModel(list.get(position));
            mBinding.setViewModel(mApiItemViewModel);
            mBinding.executePendingBindings();
        }
    }
}