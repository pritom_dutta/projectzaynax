package com.pritom.baseproject.data;

import android.content.Context;

import com.google.gson.Gson;
import com.pritom.baseproject.data.local.db.DbHelper;
import com.pritom.baseproject.data.local.prefs.PreferencesHelper;
import com.pritom.baseproject.data.model.api.ApiResponse;
import com.pritom.baseproject.data.remote.ApiHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final ApiHelper mApiHelper;

    private final Context mContext;

    private final DbHelper mDbHelper;

    private final Gson mGson;

    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public AppDataManager(Context context, DbHelper dbHelper, PreferencesHelper preferencesHelper, ApiHelper apiHelper, Gson gson) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
        mGson = gson;
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public Long getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }

    @Override
    public String getPhone() {
        return mPreferencesHelper.getPhone();
    }

    @Override
    public void setPhone(String phone) {
        mPreferencesHelper.setPhone(phone);
    }

    @Override
    public String getPassword() {
        return mPreferencesHelper.getPassword();
    }

    @Override
    public void setPassword(String password) {
        mPreferencesHelper.setPassword(password);
    }

    @Override
    public String getDOB() {
        return mPreferencesHelper.getDOB();
    }

    @Override
    public void setDOB(String DOB) {
        mPreferencesHelper.setDOB(DOB);
    }

    @Override
    public String getGender() {
        return mPreferencesHelper.getGender();
    }

    @Override
    public void setGender(String Gender) {
        mPreferencesHelper.setGender(Gender);
    }

    @Override
    public String getBlood() {
        return mPreferencesHelper.getBlood();
    }

    @Override
    public void setBlood(String Blood) {
        mPreferencesHelper.setBlood(Blood);
    }

    @Override
    public boolean getQualification() {
        return mPreferencesHelper.getQualification();
    }

    @Override
    public void setQualification(boolean qualification) {
        mPreferencesHelper.setQualification(qualification);
    }

    @Override
    public String getInsuranceName() {
        return mPreferencesHelper.getInsuranceName();
    }

    @Override
    public void setInsuranceName(String name) {
        mPreferencesHelper.setInsuranceName(name);
    }

    @Override
    public String getInsuranceExpireDate() {
        return mPreferencesHelper.getInsuranceExpireDate();
    }

    @Override
    public void setInsuranceExpireDate(String expireDate) {
        mPreferencesHelper.setInsuranceExpireDate(expireDate);
    }

    @Override
    public String getRoadsideNumber() {
        return mPreferencesHelper.getRoadsideNumber();
    }

    @Override
    public void setRoadsideNumber(String roadsideNumber) {
        mPreferencesHelper.setRoadsideNumber(roadsideNumber);
    }

    @Override
    public int getMyCarID() {
        return mPreferencesHelper.getMyCarID();
    }

    @Override
    public void setMyCarID(int id) {
        mPreferencesHelper.setMyCarID(id);
    }

    @Override
    public Boolean getNotificationByMail() {
        return mPreferencesHelper.getNotificationByMail();
    }

    @Override
    public void setNotificationByMail(Boolean notificationByMail) {
        mPreferencesHelper.setNotificationByMail(notificationByMail);
    }

    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(
                null,
                null,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null,
                null,
                null);
    }

    @Override
    public void updateUserInfo(
            String accessToken,
            Long userId,
            LoggedInMode loggedInMode,
            String userName,
            String email,
            String phone,
            String password,
            String profilePicPath) {

        setAccessToken(accessToken);
        setCurrentUserId(userId);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentUserName(userName);
        setCurrentUserEmail(email);
        setPhone(phone);
        setPassword(password);
        setCurrentUserProfilePicUrl(profilePicPath);

        // updateApiHeader(userId, accessToken);
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        //mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }


    @Override
    public void updateApiHeader(Long userId, String accessToken) {
//        mApiHelper.getApiHeader().getProtectedApiHeader().setUserId(userId);
//        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public Single<ApiResponse> doGetData(int pageNumber) {
        return mApiHelper.doGetData(pageNumber);
    }

//   TODO:- Api Call


}
