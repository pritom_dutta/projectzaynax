package com.pritom.baseproject.data.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.pritom.baseproject.data.model.db.User;


@Database(entities = {User.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
}
