package com.pritom.baseproject.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;


import com.pritom.baseproject.data.DataManager;
import com.pritom.baseproject.di.PreferenceInfo;
import com.pritom.baseproject.utils.AppConstants;

import javax.inject.Inject;

public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";

    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";

    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";

    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";

    private static final String PREF_KEY_CURRENT_USER_PROFILE_PIC_URL = "PREF_KEY_CURRENT_USER_PROFILE_PIC_URL";

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";

    private static final String PREF_KEY_USER_PHONE = "PREF_KEY_USER_PHONE";

    private static final String PREF_KEY_USER_PASSWORD = "PREF_KEY_USER_PASSWORD";

    private static final String PREF_KEY_USER_DOB = "PREF_KEY_USER_DOB";

    private static final String PREF_KEY_USER_GENDER = "PREF_KEY_USER_GENDER";

    private static final String PREF_KEY_USER_BLOOD = "PREF_KEY_USER_BLOOD";

    private static final String PREF_KEY_USER_QUALIFICATION = "PREF_KEY_USER_QUALIFICATION";

    private static final String PREF_KEY_USER_INSURANCE = "PREF_KEY_USER_INSURANCE";

    private static final String PREF_KEY_USER_INSURANCE_EXPIRE_DATE = "PREF_KEY_USER_INSURANCE_EXPIRE_DATE";

    private static final String PREF_KEY_USER_ROADSIDE_NUMBER = "PREF_KEY_USER_ROADSIDE_NUMBER";

    private static final String PREF_KEY_USER_MY_CAR_ID = "PREF_KEY_USER_MY_CAR_ID";

    private static final String PREF_KEY_USER_NOTIFICATION_MAIL = "PREF_KEY_USER_NOTIFICATION_MAIL";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(Context context, @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, null);
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply();
    }

    @Override
    public Long getCurrentUserId() {
        long userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX);
        return userId == AppConstants.NULL_INDEX ? null : userId;
    }

    @Override
    public void setCurrentUserId(Long userId) {
        long id = userId == null ? AppConstants.NULL_INDEX : userId;
        mPrefs.edit().putLong(PREF_KEY_CURRENT_USER_ID, id).apply();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE, DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public String getCurrentUserName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, null);
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, userName).apply();
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, null);
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, profilePicUrl).apply();
    }

    @Override
    public String getPhone() {
        return mPrefs.getString(PREF_KEY_USER_PHONE, null);
    }

    @Override
    public void setPhone(String phone) {
        mPrefs.edit().putString(PREF_KEY_USER_PHONE, phone).apply();
    }

    @Override
    public String getPassword() {
        return mPrefs.getString(PREF_KEY_USER_PASSWORD, null);
    }

    @Override
    public void setPassword(String password) {
        mPrefs.edit().putString(PREF_KEY_USER_PASSWORD, password).apply();
    }

    @Override
    public String getDOB() {
        return mPrefs.getString(PREF_KEY_USER_DOB, null);
    }

    @Override
    public void setDOB(String DOB) {
        mPrefs.edit().putString(PREF_KEY_USER_DOB, DOB).apply();
    }

    @Override
    public String getGender() {
        return mPrefs.getString(PREF_KEY_USER_GENDER, null);
    }

    @Override
    public void setGender(String Gender) {
        mPrefs.edit().putString(PREF_KEY_USER_GENDER, Gender).apply();
    }

    @Override
    public String getBlood() {
        return mPrefs.getString(PREF_KEY_USER_BLOOD, null);
    }

    @Override
    public void setBlood(String Blood) {
        mPrefs.edit().putString(PREF_KEY_USER_BLOOD, Blood).apply();
    }

    @Override
    public boolean getQualification() {
        return mPrefs.getBoolean(PREF_KEY_USER_QUALIFICATION, false);
    }

    @Override
    public void setQualification(boolean qualification) {
        mPrefs.edit().putBoolean(PREF_KEY_USER_QUALIFICATION, qualification).apply();
    }

    @Override
    public String getInsuranceName() {
        return mPrefs.getString(PREF_KEY_USER_INSURANCE, null);
    }

    @Override
    public void setInsuranceName(String name) {
        mPrefs.edit().putString(PREF_KEY_USER_INSURANCE, name).apply();
    }

    @Override
    public String getInsuranceExpireDate() {
        return mPrefs.getString(PREF_KEY_USER_INSURANCE_EXPIRE_DATE, null);
    }

    @Override
    public void setInsuranceExpireDate(String expireDate) {
        mPrefs.edit().putString(PREF_KEY_USER_INSURANCE_EXPIRE_DATE, expireDate).apply();
    }

    @Override
    public String getRoadsideNumber() {
        return mPrefs.getString(PREF_KEY_USER_ROADSIDE_NUMBER, null);
    }

    @Override
    public void setRoadsideNumber(String roadsideNumber) {
        mPrefs.edit().putString(PREF_KEY_USER_ROADSIDE_NUMBER, roadsideNumber+"").apply();
    }

    @Override
    public int getMyCarID() {
        return mPrefs.getInt(PREF_KEY_USER_MY_CAR_ID, 0);
    }

    @Override
    public void setMyCarID(int id) {
        mPrefs.edit().putInt(PREF_KEY_USER_MY_CAR_ID, id).apply();
    }

    @Override
    public Boolean getNotificationByMail() {
        return mPrefs.getBoolean(PREF_KEY_USER_NOTIFICATION_MAIL, false);
    }

    @Override
    public void setNotificationByMail(Boolean notificationByMail) {
        mPrefs.edit().putBoolean(PREF_KEY_USER_NOTIFICATION_MAIL, notificationByMail).apply();
    }
}
