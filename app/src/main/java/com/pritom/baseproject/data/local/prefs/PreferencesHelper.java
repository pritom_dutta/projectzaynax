package com.pritom.baseproject.data.local.prefs;


import com.pritom.baseproject.data.DataManager;

public interface PreferencesHelper {

    String getAccessToken();

    void setAccessToken(String accessToken);

    String getCurrentUserEmail();

    void setCurrentUserEmail(String email);

    Long getCurrentUserId();

    void setCurrentUserId(Long userId);

    int getCurrentUserLoggedInMode();

    void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode);

    String getCurrentUserName();

    void setCurrentUserName(String userName);

    String getCurrentUserProfilePicUrl();

    void setCurrentUserProfilePicUrl(String profilePicUrl);

    String getPhone();

    void setPhone(String phone);

    String getPassword();

    void setPassword(String password);

    String getDOB();

    void setDOB(String DOB);

    String getGender();

    void setGender(String Gender);

    String getBlood();

    void setBlood(String Blood);

    boolean getQualification();

    void setQualification(boolean qualification);

    String getInsuranceName();

    void setInsuranceName(String name);

    String getInsuranceExpireDate();

    void setInsuranceExpireDate(String expireDate);

    String getRoadsideNumber();

    void setRoadsideNumber(String roadsideNumber);

    int getMyCarID();

    void setMyCarID(int id);

    Boolean getNotificationByMail();

    void setNotificationByMail(Boolean notificationByMail);

}
