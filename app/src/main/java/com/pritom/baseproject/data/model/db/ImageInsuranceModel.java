package com.pritom.baseproject.data.model.db;

import java.io.File;

public class ImageInsuranceModel {
    private File img;
    private int isFrontOrBack;

    public ImageInsuranceModel(File img, int isFrontOrBack) {
        this.img = img;
        this.isFrontOrBack = isFrontOrBack;
    }

    public File getImg() {
        return img;
    }

    public void setImg(File img) {
        this.img = img;
    }

    public int getIsFrontOrBack() {
        return isFrontOrBack;
    }

    public void setIsFrontOrBack(int isFrontOrBack) {
        this.isFrontOrBack = isFrontOrBack;
    }


}
