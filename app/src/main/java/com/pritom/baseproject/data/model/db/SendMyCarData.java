package com.pritom.baseproject.data.model.db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SendMyCarData {

    @SerializedName("carId")
    @Expose
    private Integer carId;
    @SerializedName("vehicleBrandId")
    @Expose
    private Integer vehicleBrandId;
    @SerializedName("vehicleModelId")
    @Expose
    private Integer vehicleModelId;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("vehicleNickName")
    @Expose
    private String vehicleNickName;
    @SerializedName("emirateId")
    @Expose
    private Integer emirateId;
    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("plateNumber")
    @Expose
    private String plateNumber;
    @SerializedName("insuranceType")
    @Expose
    private String insuranceType;
    @SerializedName("insuranceNumber")
    @Expose
    private String insuranceNumber;
    @SerializedName("assistanceNo")
    @Expose
    private String assistanceNo;
    @SerializedName("insuranceExpireDate")
    @Expose
    private String insuranceExpireDate;
    @SerializedName("image")
    @Expose
    private List<String> image = null;

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getVehicleBrandId() {
        return vehicleBrandId;
    }

    public void setVehicleBrandId(Integer vehicleBrandId) {
        this.vehicleBrandId = vehicleBrandId;
    }

    public Integer getVehicleModelId() {
        return vehicleModelId;
    }

    public void setVehicleModelId(Integer vehicleModelId) {
        this.vehicleModelId = vehicleModelId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVehicleNickName() {
        return vehicleNickName;
    }

    public void setVehicleNickName(String vehicleNickName) {
        this.vehicleNickName = vehicleNickName;
    }

    public Integer getEmirateId() {
        return emirateId;
    }

    public void setEmirateId(Integer emirateId) {
        this.emirateId = emirateId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getAssistanceNo() {
        return assistanceNo;
    }

    public void setAssistanceNo(String assistanceNo) {
        this.assistanceNo = assistanceNo;
    }

    public String getInsuranceExpireDate() {
        return insuranceExpireDate;
    }

    public void setInsuranceExpireDate(String insuranceExpireDate) {
        this.insuranceExpireDate = insuranceExpireDate;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "{" +
                "carId=" + carId +
                ", vehicleBrandId=" + vehicleBrandId +
                ", vehicleModelId=" + vehicleModelId +
                ", year='" + year + '\'' +
                ", vehicleNickName='" + vehicleNickName + '\'' +
                ", emirateId=" + emirateId +
                ", customerId=" + customerId +
                ", plateNumber='" + plateNumber + '\'' +
                ", insuranceType='" + insuranceType + '\'' +
                ", insuranceNumber='" + insuranceNumber + '\'' +
                ", assistanceNo='" + assistanceNo + '\'' +
                ", insuranceExpireDate='" + insuranceExpireDate + '\'' +
                ", image=" + image +
                '}';
    }
}