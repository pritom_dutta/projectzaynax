package com.pritom.baseproject.data.remote;


import com.pritom.baseproject.data.model.api.ApiResponse;

import io.reactivex.Single;

public interface ApiHelper {

    Single<ApiResponse> doGetData(int pageNumber);

}
