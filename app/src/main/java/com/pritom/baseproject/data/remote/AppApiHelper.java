package com.pritom.baseproject.data.remote;


import com.pritom.baseproject.data.model.api.ApiResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppApiHelper implements ApiHelper {

    @Inject
    public AppApiHelper() {

    }

    @Override
    public Single<ApiResponse> doGetData(int pageNumber) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_DATA+""+pageNumber)
                .build()
                .getObjectSingle(ApiResponse.class);
    }


}
