package com.pritom.baseproject.di.component;



import com.pritom.baseproject.di.module.ActivityModule;
import com.pritom.baseproject.di.scope.ActivityScope;
import com.pritom.baseproject.ui.activity.main.MainActivity;

import dagger.Component;

@ActivityScope
@Component(modules = ActivityModule.class, dependencies = AppComponent.class)
public interface ActivityComponent {

    void inject(MainActivity activity);


}
