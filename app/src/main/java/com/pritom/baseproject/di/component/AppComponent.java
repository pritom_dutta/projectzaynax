package com.pritom.baseproject.di.component;

import android.app.Application;

import com.pritom.baseproject.MvvmApp;
import com.pritom.baseproject.data.DataManager;
import com.pritom.baseproject.di.module.AppModule;
import com.pritom.baseproject.utils.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(MvvmApp app);

    DataManager getDataManager();

    SchedulerProvider getSchedulerProvider();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }
}
