package com.pritom.baseproject.di.component;



import com.pritom.baseproject.di.module.DialogModule;
import com.pritom.baseproject.di.scope.DialogScope;

import dagger.Component;

@DialogScope
@Component(modules = DialogModule.class, dependencies = AppComponent.class)
public interface DialogComponent {

//    void inject(CityFragment dialog);


}