package com.pritom.baseproject.di.component;


import com.pritom.baseproject.di.module.FragmentModule;
import com.pritom.baseproject.di.scope.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(modules = FragmentModule.class, dependencies = AppComponent.class)
public interface FragmentComponent {
//    void inject(HomeFragment fragment);

}
