package com.pritom.baseproject.di.module;

import androidx.core.util.Supplier;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pritom.baseproject.ViewModelProviderFactory;
import com.pritom.baseproject.adapters.BaseApiAdapter;
import com.pritom.baseproject.data.DataManager;
import com.pritom.baseproject.ui.activity.main.MainViewModel;
import com.pritom.baseproject.ui.base.BaseActivity;
import com.pritom.baseproject.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private BaseActivity<?, ?> activity;

    public ActivityModule(BaseActivity<?, ?> activity) {
        this.activity = activity;
    }


    //  TODO:- LayoutManager
    @Provides
    LinearLayoutManager provideLinearLayoutManager() {
        return new LinearLayoutManager(activity);
    }

    @Provides
    GridLayoutManager provideGridLayoutManager() {
        return new GridLayoutManager(activity, 4);
    }

    //  TODO:- ViewModel

    @Provides
    MainViewModel provideMainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        Supplier<MainViewModel> supplier = () -> new MainViewModel(dataManager, schedulerProvider);
        ViewModelProviderFactory<MainViewModel> factory = new ViewModelProviderFactory<>(MainViewModel.class, supplier);
        return new ViewModelProvider(activity, factory).get(MainViewModel.class);
    }


    //  TODO:- Adapter
    @Provides
    BaseApiAdapter provideBaseApiAdapter() {
        return new BaseApiAdapter(new ArrayList<>());
    }

}
