package com.pritom.baseproject.di.module;

import androidx.core.util.Supplier;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.pritom.baseproject.ui.base.BaseDialog;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class DialogModule {

    private BaseDialog dialog;

    public DialogModule(BaseDialog dialog) {
        this.dialog = dialog;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager() {
        return new LinearLayoutManager(dialog.getActivity());
    }

//    @Provides
//    CityViewModel provideCityViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
//        Supplier<CityViewModel> supplier = () -> new CityViewModel(dataManager, schedulerProvider);
//        ViewModelProviderFactory<CityViewModel> factory = new ViewModelProviderFactory<>(CityViewModel.class, supplier);
//        return new ViewModelProvider(dialog.getActivity(), factory).get(CityViewModel.class);
//    }
//
//    //  Adapter
//    @Provides
//    CityAdapter provideCityAdapter() {
//        return new CityAdapter(new ArrayList<>());
//    }

}
