package com.pritom.baseproject.di.module;

import androidx.core.util.Supplier;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.pritom.baseproject.ui.base.BaseFragment;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {

    private BaseFragment<?, ?> fragment;

    public FragmentModule(BaseFragment<?, ?> fragment) {
        this.fragment = fragment;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager() {
        return new LinearLayoutManager(fragment.getActivity());
    }

    @Provides
    GridLayoutManager provideGridLayoutManager() {
        return new GridLayoutManager(fragment.getActivity(), 4);
    }

    //  TODO:- VIEW MODEL
//
//    @Provides
//    HomeViewModel provideHomeViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
//        Supplier<HomeViewModel> supplier = () -> new HomeViewModel(dataManager, schedulerProvider);
//        ViewModelProviderFactory<HomeViewModel> factory = new ViewModelProviderFactory<>(HomeViewModel.class, supplier);
//        return new ViewModelProvider(fragment, factory).get(HomeViewModel.class);
//    }
//
//
//    //  TODO:- Adapter
//    @Provides
//    ActiveRequestAdapter provideActiveRequestAdapter() {
//        return new ActiveRequestAdapter(new ArrayList<>());
//    }

}
