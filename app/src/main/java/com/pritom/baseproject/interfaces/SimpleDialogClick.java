package com.pritom.baseproject.interfaces;

public interface SimpleDialogClick {
    void onClose();
}