package com.pritom.baseproject.ui.activity.main;

import androidx.databinding.ObservableField;

import com.pritom.baseproject.data.model.api.ItemModel;

public class ApiItemViewModel {


    public final ObservableField<String> name;
    public final ObservableField<String> id;
    public final ObservableField<String> phone;
    public final ItemModel itemModel;

    public ApiItemViewModel(ItemModel itemModel) {
        this.itemModel = itemModel;
        this.name = new ObservableField<>(itemModel.getName());
        this.id = new ObservableField<>("ID: "+itemModel.getId());
        this.phone = new ObservableField<>(itemModel.getPhoneNumber());
    }
}