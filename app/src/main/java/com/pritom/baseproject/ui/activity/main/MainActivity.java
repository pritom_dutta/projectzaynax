package com.pritom.baseproject.ui.activity.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.pritom.baseproject.BR;
import com.pritom.baseproject.R;
import com.pritom.baseproject.adapters.BaseApiAdapter;
import com.pritom.baseproject.data.model.api.ApiResponse;
import com.pritom.baseproject.databinding.ActivityMainBinding;
import com.pritom.baseproject.di.component.ActivityComponent;
import com.pritom.baseproject.helper.EqualSpacingItemDecoration;
import com.pritom.baseproject.ui.base.BaseActivity;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainNavigator {

    ActivityMainBinding mActivityMainBinding;

    @Inject
    BaseApiAdapter mBaseApiAdapter;
    @Inject
    LinearLayoutManager mLayoutManager;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityMainBinding = getViewDataBinding();
        mViewModel.setNavigator(this);
        setUp();

    }

    private void setUp() {
        //showLoading();
        mViewModel.getData(1);

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mActivityMainBinding.rvList.setLayoutManager(mLayoutManager);
        mActivityMainBinding.rvList.addItemDecoration(new EqualSpacingItemDecoration(40));
        mActivityMainBinding.rvList.setItemAnimator(new DefaultItemAnimator());
        mActivityMainBinding.rvList.setAdapter(mBaseApiAdapter);

//        mGridLayoutManager.setSpanCount(4);
//        mFragmentFindVideoCourseBinding.rvVideoCourse.setLayoutManager(mGridLayoutManager);
//        int spanCount = 4;
//        int spacing = 10;
//        boolean includeEdge = true;
//        mFragmentFindVideoCourseBinding.rvVideoCourse.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
//        mFragmentFindVideoCourseBinding.rvVideoCourse.setAdapter(mFindVideoCourseAdapter);
    }

    @Override
    public void performDependencyInjection(ActivityComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public void handleError(Throwable throwable) {
        hideLoading();
        if (throwable instanceof ANError) {
            ANError anError = (ANError) throwable;
            handleApiError(anError);
        }
    }

    @Override
    public void onSuccessData(ApiResponse mApiResponse) {
        hideLoading();
        if(mApiResponse.getSuccess()){
            mBaseApiAdapter.addItems(mApiResponse.getData().getItems());
        }else{
            onError(mApiResponse.getMessage());
        }

    }
}