package com.pritom.baseproject.ui.activity.main;

import com.pritom.baseproject.data.model.api.ApiResponse;

public interface MainNavigator {
    void handleError(Throwable throwable);

    void onSuccessData(ApiResponse mApiResponse);
}
