package com.pritom.baseproject.ui.activity.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pritom.baseproject.data.DataManager;
import com.pritom.baseproject.ui.base.BaseViewModel;
import com.pritom.baseproject.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

public class MainViewModel extends BaseViewModel<MainNavigator> {

//    private final MutableLiveData<List<String>> listMutableLiveData;

    @Inject
    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void getData(int pageNumber) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doGetData(pageNumber)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().onSuccessData(response);
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

//    public LiveData<List<String>> getLowSectionListLiveData() {
//        return listMutableLiveDataLow;
//    }
}
