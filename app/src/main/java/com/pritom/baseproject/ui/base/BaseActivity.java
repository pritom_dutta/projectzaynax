

package com.pritom.baseproject.ui.base;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.common.ANConstants;
import com.androidnetworking.error.ANError;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.pritom.baseproject.MvvmApp;
import com.pritom.baseproject.R;
import com.pritom.baseproject.data.DataManager;
import com.pritom.baseproject.data.model.api.ApiError;
import com.pritom.baseproject.di.component.ActivityComponent;
import com.pritom.baseproject.di.component.DaggerActivityComponent;
import com.pritom.baseproject.di.module.ActivityModule;
import com.pritom.baseproject.interfaces.SimpleDialogClick;
import com.pritom.baseproject.utils.AppConstants;
import com.pritom.baseproject.utils.CommonUtils;
import com.pritom.baseproject.utils.NetworkUtils;


import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;


public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity
        implements BaseFragment.Callback {

    // TODO
    // this can probably depend on isLoading variable of BaseViewModel,
    // since its going to be common for all the activities

    @Inject
    DataManager dataManager;

    private ProgressDialog mProgressDialog;

    private T mViewDataBinding;

    @Inject
    protected V mViewModel;

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();


    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection(getBuildComponent());
        super.onCreate(savedInstanceState);
        performDataBinding();
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                ContextCompat.checkSelfPermission(
                        getApplicationContext(), permission) ==
                        PackageManager.PERMISSION_GRANTED;
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && CommonUtils.scaleDown != null) {
            mProgressDialog.cancel();
            CommonUtils.scaleDown.cancel();
        }
    }

    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    public void openActivityOnTokenExpire() {
//        startActivity(LoginActivity.newIntent(this));
//        finish();
    }

    private ActivityComponent getBuildComponent() {
        return DaggerActivityComponent.builder()
                .appComponent(((MvvmApp) getApplication()).appComponent)
                .activityModule(new ActivityModule(this))
                .build();
    }

    public abstract void performDependencyInjection(ActivityComponent buildComponent);

    public void over_R_Permission(){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//            if(!Environment.isExternalStorageManager()){
//                Snackbar.make(findViewById(android.R.id.content), "Permission needed!", Snackbar.LENGTH_INDEFINITE)
//                        .setAction("Settings", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                try {
//                                    Uri uri = Uri.parse("package:" + BuildConfig.APPLICATION_ID);
//                                    Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, uri);
//                                    startActivity(intent);
//                                } catch (Exception ex){
//                                    Intent intent = new Intent();
//                                    intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
//                                    startActivity(intent);
//                                }
//                            }
//                        })
//                        .show();
//            }
//        }
    }

    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, permissions,requestCode);
        }
    }

    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    public void openDialog(String msg, SimpleDialogClick mSimpleDialogClick) {
//        final Dialog dialog = new Dialog(this);
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.mez_dialog);
//
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        text.setText(msg);
//
//        AppCompatImageView dialogButton = dialog.findViewById(R.id.img_close);
//        dialogButton.setOnClickListener(v -> {
//            dialog.dismiss();
//            mSimpleDialogClick.onClose();
//        });
//
//        dialog.show();
    }

    public void openNoInternetDialog(SimpleDialogClick mSimpleDialogClick) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.no_internet_dialog);

        AppCompatButton btnTry = dialog.findViewById(R.id.btn_try);
        btnTry.setOnClickListener(v -> {
            if (isNetworkConnected()) {
                dialog.dismiss();
                mSimpleDialogClick.onClose();
            }
        });
        dialog.show();
    }
//
//
//    public void openDialog(String msg, PackageModel packageModel, BuyPackageDialogClick mBuyPackageDialogClick) {
//        final Dialog dialog = new Dialog(this);
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.buy_package_dialog);
//
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        TextView txtPrice = (TextView) dialog.findViewById(R.id.tv_price);
//        TextView txtDiscountPrice = (TextView) dialog.findViewById(R.id.tv_discount_price);
//        TextView tvValidity = (TextView) dialog.findViewById(R.id.tv_validity);
//
//        if (packageModel.getDuration() != null) {
//            String duration = (Integer.parseInt(packageModel.getDuration()) / 30) + "";
//            tvValidity.setText(getString(R.string.validity, CommonUtils.getBDDigit(duration)));
//        }
//
//        if (packageModel.getDiscount() != null) {
//            txtPrice.setText(CommonUtils.getBDDigit(packageModel.getDiscount()));
//            txtDiscountPrice.setText(CommonUtils.getBDDigit(packageModel.getPrice()));
//        } else {
//            txtPrice.setText(CommonUtils.getBDDigit(packageModel.getPrice()));
//            txtDiscountPrice.setText("");
//        }
//        text.setText(msg);
//
//        ImageView dialogButton = dialog.findViewById(R.id.img_close);
//        AppCompatButton btnBuy = dialog.findViewById(R.id.btn_buy);
//
//        btnBuy.setOnClickListener(v -> {
//            dialog.dismiss();
//            mBuyPackageDialogClick.onPackage(packageModel);
//        });
//
//        dialogButton.setOnClickListener(v -> {
//            dialog.dismiss();
//            mBuyPackageDialogClick.onClose();
//        });
//
//        dialog.show();
//    }
//
//    public void openDialogForStartExam(String msg, StartExam mStartExam) {
//        final Dialog dialog = new Dialog(this);
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.start_exam_dialog);
//
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        text.setText(msg);
//
//        ImageView dialogButton = dialog.findViewById(R.id.img_close);
//        AppCompatButton btnBuy = dialog.findViewById(R.id.btn_buy);
//
//        btnBuy.setOnClickListener(v -> {
//            dialog.dismiss();
//            mStartExam.onSureWantExam();
//        });
//
//        dialogButton.setOnClickListener(v -> {
//            dialog.dismiss();
//            mStartExam.onClose();
//        });
//
//        dialog.show();
//    }

    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar(getString(R.string.some_error));
        }
    }

    public void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView
                .findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, R.color.white));
        snackbar.show();
    }

    public void handleApiError(ANError error) {

        if (error.getErrorBody() == null) {
            Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
            return;
        }

        if (error == null || error.getErrorBody() == null) {
            //getMvpView().onError(R.string.api_default_error);
            Toast.makeText(this, "" + R.string.api_default_error, Toast.LENGTH_SHORT).show();
            return;
        }

        if (error.getErrorCode() == AppConstants.API_STATUS_CODE_LOCAL_ERROR
                && error.getErrorDetail().equals(ANConstants.CONNECTION_ERROR)) {
            //getMvpView().onError(R.string.connection_error);
            Toast.makeText(this, "" + R.string.connection_error, Toast.LENGTH_SHORT).show();
            return;
        }

        if (error.getErrorCode() == AppConstants.API_STATUS_CODE_LOCAL_ERROR
                && error.getErrorDetail().equals(ANConstants.REQUEST_CANCELLED_ERROR)) {
            //getMvpView().onError(R.string.api_retry_error);
            Toast.makeText(this, "" + R.string.api_retry_error, Toast.LENGTH_SHORT).show();
            return;
        }

        final GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();


        try {
            ApiError apiError = gson.fromJson(error.getErrorBody(), ApiError.class);
//
            if (apiError == null || apiError.getMessage() == null) {
                Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "" + R.string.api_default_error, Toast.LENGTH_SHORT).show();
                return;
            }

            switch (error.getErrorCode()) {
                case HttpsURLConnection.HTTP_UNAUTHORIZED:
                    openActivityOnTokenExpire();
                    break;
                case HttpsURLConnection.HTTP_FORBIDDEN:
                case HttpsURLConnection.HTTP_INTERNAL_ERROR:
                case HttpsURLConnection.HTTP_NOT_FOUND:
                default:
                    //Log.e("ERROR", "handleApiError"+ apiError.getMessage());
                    //Toast.makeText(this, "" + apiError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (JsonSyntaxException | NullPointerException e) {
            Log.e("ERROR", "handleApiError" + error.getErrorBody());
            // Log.e(TAG, "handleApiError", e);
            //getMvpView().onError(R.string.api_default_error);
        }

    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
    }

    public void wrapTabIndicatorToTitle(TabLayout tabLayout, int externalMargin, int internalMargin) {
        View tabStrip = tabLayout.getChildAt(0);
        if (tabStrip instanceof ViewGroup) {
            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
            int childCount = ((ViewGroup) tabStrip).getChildCount();
            for (int i = 0; i < childCount; i++) {
                View tabView = tabStripGroup.getChildAt(i);
                //set minimum width to 0 for instead for small texts, indicator is not wrapped as expected
                tabView.setMinimumWidth(0);
                // set padding to 0 for wrapping indicator as title
                tabView.setPadding(0, tabView.getPaddingTop(), 0, tabView.getPaddingBottom());
                // setting custom margin between tabs
                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) tabView.getLayoutParams();
                    if (i == 0) {
                        // left
                        settingMargin(layoutParams, externalMargin, internalMargin);
                    } else if (i == childCount - 1) {
                        // right
                        settingMargin(layoutParams, internalMargin, externalMargin);
                    } else {
                        // internal
                        settingMargin(layoutParams, internalMargin, internalMargin);
                    }
                }
            }

            tabLayout.requestLayout();
        }
    }

    private void settingMargin(ViewGroup.MarginLayoutParams layoutParams, int start, int end) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            layoutParams.setMarginStart(start);
            layoutParams.setMarginEnd(end);
            layoutParams.leftMargin = start;
            layoutParams.rightMargin = end;
        } else {
            layoutParams.leftMargin = start;
            layoutParams.rightMargin = end;
        }
    }


    public void openDrawer() {

    }

    public void closeDrawer() {

    }

    public void openMyRequest(){

    }

    public void signOut() {
//        LayoutInflater inflater = getLayoutInflater();
//        View view = inflater.inflate(R.layout.logout_layout, null);
//
//        new AlertDialog.Builder(this)
//                .setMessage(getString(R.string.sign_out_prompt))
//                .setCustomTitle(view)
//                .setPositiveButton(R.string.yes, (dialog, which) -> {
//                    dataManager.setUserAsLoggedOut();
//                    openActivityOnTokenExpire();
//                })
//                .setNegativeButton(R.string.no, null)
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
    }

    public void exitApp() {
//        new AlertDialog.Builder(this)
//                .setMessage(getString(R.string.exit_app_prompt))
//                .setPositiveButton(R.string.yes, (dialog, which) -> {
//                    finish();
//                })
//                .setNegativeButton(R.string.no, null)
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
    }

}

