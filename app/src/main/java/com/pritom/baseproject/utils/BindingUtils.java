/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.pritom.baseproject.utils;

import android.content.Context;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.BindingAdapter;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.text.Html;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pritom.baseproject.R;


public final class BindingUtils {

    private BindingUtils() {
        // This class is not publicly instantiable
    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
//        Glide.with(context).load(url).placeholder(R.drawable.logo).into(imageView);
    }

    @BindingAdapter("imageUrlTwo")
    public static void setImageUrlTwo(AppCompatImageView imageView, String url) {
        Context context = imageView.getContext();
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        Glide.with(context).load(url).placeholder(circularProgressDrawable).into(imageView);
    }

    @BindingAdapter("set_base_image")
    public static void basetoimage(ImageView imageView, String url) {
        Bitmap myBitmap = BitmapFactory.decodeFile(url);
//        Glide.with(imageView.getContext()).load(myBitmap).placeholder(R.drawable.ic_baseline_image).into(imageView);
    }

    @BindingAdapter("text_html")
    public static void setTextHtml(TextView textView, String string) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            textView.setText(Html.fromHtml(textView.getContext().getString(R.string.amount_with_value, string), Html.FROM_HTML_MODE_COMPACT));
//        } else {
//            textView.setText(Html.fromHtml(textView.getContext().getString(R.string.amount_with_value, string)));
//        }
    }

}
